---
title: "DP_Project_89181105"
author: '89181105'
date: "31/08/2021"
output: 
  html_document:
  theme: readable
##bibliography: references.bib
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

##library(bibtex)
library(dplyr)
library(ggplot2)
library(RColorBrewer)
library(fmsb)

```

**Disclaimer:** Due to the lack of availability, the dataset I will be using is a combined set of a World Happiness Report from 2021, a Population dataset from 2020 and a Life Expectancy dataset from 2018. Therefore a handful of countries may be missing in the data. My apologies to them.

```{r data, echo=FALSE, warning=FALSE}


happy <- read.csv("world_happiness_report_2021.csv")
pop <- read.csv("population_by_country_2020.csv")
life <- read.csv("life_expectancy.csv")

colnames(happy)[1] <- c("Country")
colnames(pop)[1] <- c("Country")

mdata <- merge(happy, pop, by="Country")
mdata <- merge(mdata, life, by="Country")

data <- data.frame(mdata$Country, mdata$Continent, mdata$Regional.indicator, mdata$Ladder.score, mdata$Population..2020., mdata$Fert..Rate, mdata$Yearly.Change, mdata$Net.Change, mdata$`Density..P.KmÂ².`, mdata$Urban.Pop.., mdata$Overall.Life, mdata$Male.Life, mdata$Female.Life, mdata$Logged.GDP.per.capita, mdata$World.Share, mdata$Social.support, mdata$Freedom.to.make.life.choices, mdata$Generosity, mdata$Perceptions.of.corruption, mdata$Migrants..net.)

colnames(data) <- c("Country", "Continent", "Regional Indicator", "Ladder Score", "Population", "Fertility Rate", "Pop. Change", "Net. Change", "Pop. Density", "Urban Pop.", "Avg. Lifespan", "Avg. M", "Avg. F", "GDP", "World Share", "Social Support", "Freedom", "Generosity", "Corruption", "Migrants")

ta <- which(data$Country=="United States")
data$Country[ta] <- "USA"

data$`Pop. Change` <- gsub(' %', '', as.character(data$`Pop. Change`))
data$`Pop. Change` <- as.numeric(as.character(data$`Pop. Change`))

data$`Urban Pop.` <- gsub(' %', '', as.character(data$`Urban Pop.`))
data$`Urban Pop.` <- as.numeric(as.character(data$`Urban Pop.`))

data$`World Share` <- gsub(' %', '', as.character(data$`World Share`))
data$`World Share` <- as.numeric(as.character(data$`World Share`))

data$`Fertility Rate` <- as.numeric(as.character(data$`Fertility Rate`))

Rank <- 1:nrow(data)
data <- data.frame(Rank, data[order(-data$`Ladder Score`),])



rm(happy, pop, life, mdata, ta, Rank)

```

# Is the World happy?

![](HappyEarth.jpg){width=50%}

> "Happiness is the feeling that power increases — that resistance is being overcome"

This was said by [**Friedrich Nietzsche**](https://medium.com/the-understanding-project/friedrich-nietzsche-on-the-secret-ingredient-for-happiness-643da473d919) (1844-1900), a German philosopher, cultural critic, composer, poet and writer of the late 19th century. He argued that happiness is not found by default, but is achieved as the result of hard work, which, if leading to success, brings happiness.
In some countries it is fairly easy to gain success, like, getting good grades at school, graduating from it, studying or working. In others, however, it is not and people tend to succeed less, tend to achieve less in their lifetime. Even though their wishes are probably set way lower than ours are.

```{r happiness density, echo=FALSE}

data %>%
  ggplot( aes(x=Ladder.Score)) +
  geom_density(fill='#9c0808', color='#660404', alpha=0.7) +
  ggtitle("Distribution of the Happiness Score") +
  theme_light()

```

The *World Happiness Report* is a publication of the United Nations Sustainable Development Solution Network. It contains articles and rankings of national happiness based on surveyed individuals from all over the world in rating of their own lives. Primarily, data from the Gallup World Poll is used, which has a population representation of more than 98% of adult population.

Each year, they rank the countries of the world by their reported happiness.
Aditionally, they estimate the extent to which each of six factors – economic production, social support, life expectancy, freedom, absence of corruption, and generosity – contribute to making life evaluations higher in each country than they are in Dystopia, a hypothetical country that has values equal to the World’s lowest national averages for each of the six factors. They have no impact on the total score reported for each country, but they do explain why some countries rank higher than others [@1].



```{r ranking top, echo=FALSE}

head(data[ c("Rank", "Ladder.Score", "Country")], 10)

```

In 2021, these were the top ten highest ranking countries on the World Happiness Score. **Finland** has been leading for the fourth year in a row. The World Happiness Report has found that the five Nordic countries (Finland, Denmark, Norway, Sweden and Iceland) in general have very high scores. From 2013 until today, all five of these countries found themselves in the Top 10 of the annual ranking.  
In the seventh chapter of the World Happiness Report from 2020, this phenomenon is explained by the **good quality of institutions**, such as reliable and extensive **welfare benefits**, **low corruption**, and **well-functioning democracy and state institutions**. Nordic citizens also experience a high sense of **autonomy and freedom** and a high level of **social trust** towards each other [@2].

On the other side of the scale, such conditions are unfortunately only hardly met.

```{r ranking bottom, echo=FALSE}

tail(data[ c("Rank", "Ladder.Score", "Country")], 10)

```

On the list of the ten last countries we find   
~ **Burundi**, the first country in the world to even leave the [International Criminal Court](https://www.icc-cpi.int/) after having been accused of various crimes and **human rights violations**, such as extrajudicial killings, torture, and sexual violence[@3],  
~ **Yemen**, a country on the southern end of the Arabian Peninsula, which in 2019 has been reported by the United Nations as the country with the most people in **need of humanitarian aid**, about 24 million people, or 85% of its population[@4],  
~ **Tanzania**, an East African country with plenty of famous attractions like Mount Kilimanjaro, Lake Victoria, Lake Tanganyika, and Lake Malawi, even the Ngorongoro Crater, but with a law threatening **life imprisonment for homosexuals** and the highest occurrence of human right violations among 27 African countries in which [muti](https://en.wikipedia.org/wiki/Muti) is known to be practiced. Their government have also annulled the rights of NGOs as well as individuals to directly file any case against it at the African Court for Human and People's Rights in Arusha [@5],  
~ **Haiti**, a country located on the island of Hispaniola in the Greater Antilles archipelago of the Caribbean Sea, the first island Christopher Columbus stumbled across in 1492 while searching for a new way to get to Asia but has now the lowest [Human Development Index](http://hdr.undp.org/en/content/human-development-index-hdi) in the Americas,  
~ **Malawi**, formerly known as Nyasaland, a country in Southeastern Africa with a **low life expectancy** and **high infant mortality**, as well as widespread occurrences of **HIV/AIDS**. Despite the nation giving relatively plenty effort into developing themselves, **violence against women**, **human trafficking** and **child labour** are, among others, still huge problems.  
~ **Lesotho**, one out of three enclaved countries in this world, surrounded entirely by South Africa, in which nearly a quarter of the population tests positive for **HIV** [@6] and which, despite holding one of the highest literacy rates in Africa with 85% of female and 68% of male literacy, suffers from **high unemployment**, **economic collapse**, a **weak currency** and poor **travel documents** restricting movement.  
~ **Botswana**, a landlocked country of Southern Africa with one of the world's fastest-growing economies, dominated by mining, cattle, and tourism. They, too, suffer from an extremely big **HIV/AIDS** infection rate of 20% [@7] and have a lot of dissatisfied native citizens that the government relocated to build nature reserves, leading to **high unemployment** rates and **alcoholism**. Capital punishment is Botswana's legal penalty for murder in Botswana, executions are carried out by hanging [@8].  
**Rwanda**, another landlocked country in East Africa with one of the **lowest life expectancies** in the world and although relatively low in corruption, accused multiple times for **suppression of opposition groups**, intimidation and **restrictions on freedom of speech**.  
**Zimbabwe**, which is yet another landlocked Southeast African country and was once known as the *Jewel of Africa* for its great prosperity [@9] but has since faced a steady **economic decline**, experiencing several crashes and **hyperinflation** along the way, especially during the 40 year reign of Robert Mugabe, under whom the state security apparatus dominated the country and was responsible for widespread **human rights violations** [@10].  
And last and actually kind of least, **Afghanistan**, a country at the crossroads of Central and South Asia, recently recaptured by the **Taliban**, which have been fought against in extensive battles by the US and allies from 2001 until this year. The country suffers greatly from **terrorim, poverty, child malnutrition**, and **corruption**.   



One may notice a geographical pattern in the top and the bottom of the World Happiness Ranking: Most countries in the *Top 10 are European*, while most countries ranked the *lowest are African*.
```{r continent average, echo=FALSE}

eu <- data[data$Continent=="Europe", c("Country", "Continent", "Ladder.Score", "Regional.Indicator")]
af <- data[data$Continent=="Africa", c("Country", "Continent", "Ladder.Score")]
as <- data[data$Continent=="Asia", c("Country", "Continent", "Ladder.Score")]
sa <- data[data$Continent=="South America", c("Country", "Continent", "Ladder.Score")]
na <- data[data$Continent=="North America", c("Country", "Continent", "Ladder.Score")]

val <- c(sum(eu$`Ladder.Score`)/nrow(eu), sum(af$`Ladder.Score`)/nrow(af), sum(as$`Ladder.Score`)/nrow(as), sum(sa$`Ladder.Score`)/nrow(sa), sum(na$`Ladder.Score`)/nrow(na))

continents <- c("Europe", "Africa", "Asia", "S. America", "N. America")

colour <- brewer.pal(5, "Set1")
barplot(height = val, names = continents, col = colour, ylim = c(0,8))



rm(af, as, sa, na, val, continents, colour)

```

Score averages that may not be too surprising to look at. While Europe, the continent with the overall best living conditions, scores an average of more than 6, Africa does not even reach 5.  
Even Europe is not equally happy, there are more and less developed countries, largely impacted by who they were allied to or occupied by after the Second World War [@11].

```{r europe average, echo=FALSE}

cae <- eu[eu$`Regional.Indicator`=="Central and Eastern Europe",]
we <- eu[eu$`Regional.Indicator`=="Western Europe",]
cois <- eu[eu$`Regional.Indicator`=="Commonwealth of Independent States",]

val <- c(sum(cae$`Ladder.Score`)/nrow(cae), sum(we$`Ladder.Score`)/nrow(we), sum(cois$`Ladder.Score`)/nrow(cois))
regions <- c("Central & East", "Western", "Commonwealth")

colour = brewer.pal(3, "Set2")
barplot(height = val, names = regions, col = colour, ylim = c(0,8))

rm(eu, cae, cois, val, regions, colour)

```

As predicted, *Western Europe*, which includes countries like the Nordics, Germany, England, Italy, France or Luxembourg, *towers above Central & Eastern Europe*, of which the Baltics, ExYu countries, Poland or Bulgaria are part of.  
The lowest score the countries belonging to the [Commonwealth of Independent States](http://www.cisstat.com/eng/cis.htm), which in Europe include for instance Belarus, Armenia, and Ukraine.

With a bit of political and world-economic knowledge, one may notice that these three groups of countries all have different economies that were even more different during the Cold War. The GDP of these countries is therefore up to this day recognizably different.

**But how much does the GDP impact the nation's happiness?**

We shall take a look at GDP vs Happiness in Western Europe and Africa:

```{r GDPvsHappiness WEvsAfr, echo=FALSE}

weu <- data[data$Regional.Indicator=="Western Europe", c("Ladder.Score", "GDP")]
ggplot(weu, aes(x=GDP, y=`Ladder.Score`)) +
  geom_line() +
  ggtitle("GDP vs Happiness - Western Europe")

afr <- data[data$Continent=="Africa", c("Ladder.Score", "GDP")]
ggplot (afr, aes(x=GDP, y=Ladder.Score)) +
  geom_line() +
  ggtitle("GDP vs Happiness - Africa")



rm(weu, afr)

```

These graphs are very interesting to look at. One should pay special attention to the scaling of the x-axis, as it is *much smaller* in the graph of Western Europe than in the graph of Africa, yet a pattern is visible in the first but not in the latter: While a steady growth between GDP and the ranked happiness is clearly visible in Western Europe, a rise in happiness only seems to occur after   
This may be due to how these countries **organize their wealth** and how much the country's gross domestic can really be influenced by the commoners.   
While Europeans have **plenty of opportunities** available to **spend their money** on food, clothing, imported products, electronics, luxury goods, ..., many people in Africa **cannot bring enough food home** to keep a young child alive. There are also a whole lot **less businesses** in such places because if people cannot afford to buy their products, it does not pay out for most people to open.  
There are also countries with living conditions so harsh that the GDP barely influences the common peoples' happiness. Feeling unsafe in your own home is not a good predecessor of a high ranking.

On a larger scale, however, a pattern is clearly visible:
```{r GDPvsHappiness World, echo=FALSE}

ggplot(data, aes(x=GDP, y=`Ladder.Score`)) +
  geom_line() +
  ggtitle("GDP vs Happiness - see the pattern?")

```

  
Comparing all countries of the world at once, one can see a possible correlation between the country's GDP and its happiness after all: **The higher the GDP, the higher the ranking** (on average). 

But how do other aspects influence a country's happiness? May there be a connection between the way of life and the happiness? Most Western European countries have a relatively large urban population, for instance, which allows education and job opportunities, thus possible success, thus possible happiness.

```{r Popdata1, echo=FALSE}

popdata <- data.frame(data$Rank, data$Ladder.Score, data$Country, data$Population, data$Fertility.Rate, data$Pop..Change, data$Pop..Density, data$Urban.Pop.)

colnames(popdata) <- c("Rank", "Ladder Score", "Country", "Population", "Fertility Rate", "Pop. Change", "Pop. Density", "Urban Pop.")

popdata <- na.omit(popdata)

head(popdata[order(-popdata$`Urban Pop.`), c("Rank", "Ladder Score", "Country", "Urban Pop.")], 10)
head(popdata[order(popdata$`Urban Pop.`), c("Rank", "Ladder Score", "Country", "Urban Pop.")], 10)

```
We can observe two tables, one showing the Top 10 countries with the highest **urban population** and the other showing the countries with the least. Looking at their ranks, we can quickly notice, that most countries on this list are actually in the *Top 55* of the world when it comes to happiness, except for Jordan, a relatively well developed and stable country with a rather small economy, however and a recent enormous influx of immigrants from surrounding turbulent countries, which have destabilized it somewhat [@12].
In the second table we can clearly see that apart from Niger and Nepal, **none** of the listed countries **exceed 5** on the Happiness Scale. With less than a quarter of people living in cities, the majority of these countries Cambodia and Nepal, two Asian countries, face very own struggles with **labour exploitation** and **oppression** by mainly China. Nepal is also located in the Himalayan mountains, thus the construction of large and developing cities is very difficult, the population is scattered rather scarcely.

Another aspect we may want to look into is **population density**, which may have a significant influence, too.
```{r Popdata2, echo=FALSE}

head(popdata[order(-popdata$`Pop. Density`), c("Rank", "Ladder Score", "Country", "Pop. Density")], 10)
head(popdata[order(popdata$`Pop. Density`), c("Rank", "Ladder Score", "Country", "Pop. Density")], 10)

```
These two tables show the ten countries with the densest and the least dense population. It becomes clear, that the countries and their ranks in these lists are very mixed and show basically **no pattern**. I believe it is not the space that matters, it is the usage of that space. And to some extend also its usability.  

What would be more interesting to look at would be the **fertility rates**. It is well known that developed countries like Japan, Germany, or Sweden experience **very low birth rates**. One of the major reasons for that is how **society** in developed nations changes its **view on a woman** and her role in life. While most less developed countries support **"traditional" values**, where the woman's main tasks are to be a wife, to take care of the household and to raise the children, while in **"modern" societies** a woman is more liberal to follow her own path. Another thing that influences birth rates is **infancy and maternal death**. Poor countries with **low hygiene standards** suffer from such more often and it is thus normal to have way more pregnancies than surviving children, similar as it was in Europe not too long ago. Now many of those countries are actually obtaining some sort of healthcare or at least aid, which drops infant mortality and leads to a poor family with five instead of two children, which, also, is harder to maintain. Additionally, **sex education and legal abortions** are not well spread in Africa, these topics are often tabooed or even illegal and **birth control** is also not fairly accessible, which also contributes to the extremely high HIV/AIDS infections.
```{r Popdata 3, echo=FALSE}

head(popdata[order(-popdata$`Fertility Rate`), c("Rank", "Ladder Score", "Country", "Fertility Rate", "Pop. Change")], 10)
head(popdata[order(popdata$`Fertility Rate`), c("Rank", "Ladder Score", "Country", "Fertility Rate", "Pop. Change")], 10)

```
We can see that the countries with high fertility rates are mostly very low ranking, while the countries with the lowest fertility rates are actually rather medium ranked. There may be a point at which people in developed countries decide to have more children again?
```{r Popdata plot, echo=FALSE}

ggplot(popdata, aes(x=`Ladder Score`, y=`Fertility Rate`)) +
  geom_line()



rm(popdata)

```

As this graph shows, the assumption does not hold. The average fertility rates at the higher Happiness Ranks are **generally very low** and do not appear to increase. It seems like people will just generally be appeased with smaller families of zero to three children. Although, looking back at the table with the lowest fertility rates, several of these countries aren't even terribly developed but **poor compared to their surroundings** (like Moldova, Poland, or Bosnia). Maybe people lose **perspective and hope** to be able to give a child a proper childhood and future.  
In most developed capitalistic countries, a **higher standard equals to higher prices** for groceries and supplies but **not necessarily to proportionally higher payments**. Many people able to get by on their own or with a partner may simply not be able to afford raising a child but are, due to education aware of it and can, due to the availability of sexual education and birth control prevent having children, as well.

A lower birth rate can become **dangerous** for developed countries, especially if they have good health care and provide well for the elderly. The entire population of the country **ages** gradually, which leaves more old people than young people, which leads to the tax money from the young not to suffice for the pensions of the old people, which in return leads to **senior poverty**.
```{r Average Ages, echo=FALSE}

agesa <- data.frame(data$Rank, data$Country, data$Continent, data$Ladder.Score, data$Avg..Lifespan, "Avg")
colnames(agesa) <- c("Rank", "Country", "Continent", "Ladder Score", "Avg. Lifespan", "Gender")

agesf <- ages <- data.frame(data$Rank, data$Country, data$Continent, data$Ladder.Score, data$Avg..F, "Female")
colnames(agesf) <- c("Rank", "Country", "Continent", "Ladder Score", "Avg. Lifespan", "Gender")

agesm <- ages <- data.frame(data$Rank, data$Country, data$Continent, data$Ladder.Score, data$Avg..M, "Male")
colnames(agesm) <- c("Rank", "Country", "Continent", "Ladder Score", "Avg. Lifespan", "Gender")

names(agesf) <- names(agesa)
names(agesm) <- names(agesf)

ages <- rbind(agesf, agesa, agesm)

ggplot(ages, aes(x=`Ladder Score`, y=`Avg. Lifespan`, color=Gender)) +
  geom_point() +
  scale_color_manual(values = c("#303030", "#ff80ce", "#3fd7eb"))



rm(agesa, agesf, agesm, ages)

```

  
This graph shows us the **average lifespan** of countries compared to the scoring of the corresponding country on the World Happiness Rating.
Clearly one can observe the **increase of the average lifespan with the increase of rating** but these two factors are only **indirectly linked** to each other. It is the quality of life, the health care, the hygiene standards and more that allow people to become old, as well as allow the happiness rating to grow.



Finally, we would like to look at some of the factors we have talked about so far, add a couple more and observe them in a combined and adjusted manner. Note that the following representation does not show accurate scales between the individual factors, as some are divided or multiplied by a decimal or two to be able to scale the plot.
```{r 6, echo=FALSE}

udata <- na.omit(data)
ravg <- c(sum(data$`Ladder.Score`)/nrow(data), sum(data$`Fertility.Rate`)/nrow(data), (sum(udata$`Urban.Pop.`)/nrow(udata))/10, (sum(data$`Avg..Lifespan`)/nrow(data))/10, sum(data$`GDP`)/nrow(data), (sum(data$`Freedom`)/nrow(data))*10, sum(data$Social.Support)/nrow(data)*10, sum(data$Generosity)/nrow(data)*100, sum(data$Corruption)/nrow(data)*10)

worst <- tail(data, 10)
rworst <- c(sum(worst$`Ladder.Score`)/nrow(worst), sum(worst$`Fertility.Rate`)/nrow(worst), (sum(worst$`Urban.Pop.`)/nrow(worst))/10, (sum(worst$`Avg..Lifespan`)/nrow(worst))/10, sum(worst$`GDP`)/nrow(worst), (sum(worst$`Freedom`)/nrow(worst))*10, sum(worst$Social.Support)/nrow(worst)*10, sum(worst$Generosity)/nrow(worst)*100, sum(worst$Corruption)/nrow(worst)*10)

best <- head(data, 10)
rbest <- c(sum(best$`Ladder.Score`)/nrow(best), sum(best$`Fertility.Rate`)/nrow(best), (sum(best$`Urban.Pop.`)/nrow(best))/10, (sum(best$`Avg..Lifespan`)/nrow(best))/10, sum(best$`GDP`)/nrow(best), (sum(best$`Freedom`)/nrow(best))*10, sum(best$Social.Support)/nrow(best)*10, sum(best$Generosity)/nrow(best)*100, sum(best$Corruption)/nrow(best)*10)

rdata <- rbind(rbest, rworst, ravg)

radar <- as.data.frame(matrix(rdata, ncol=length(rdata)/3))
colnames(radar) <- c("Ladder Score", "Fertility", "Urban Pop.", "Avg. Lifespan", "GDP", "Freedom", "Social Support", "Generosity", "Corruption")

radar <- rbind(rep(10, 5), rep(0,5), radar)

rcolors <- c('#1a9c11', '#ed0e0e', '#000000')

radarchart(radar, pcol=rcolors, cglcol="gray", cglwd=0.5)



rm(udata, ravg, worst, rworst, best, rbest, rdata, radar, rcolors)
rm(data)

```
  
This radar chart, showing the **worldwide average** in black, the **Top 10** in green and the **Low 10** averages in red, helps us understand how significantly some factors impact our perception of happiness. *A happy country appears to be marked by low corruption, high generosity, better social support, more freedom, a higher GDP, a higher average lifespan, a denser urban population and lower fertility rates.*  
For many of us this sounds like the normal life we are used to and live day to day. But for an enormous amount of humans out there this kind of life is an **unreachable dream**, even the thought of being able to lavishly enjoy three meals a day can move them to cross entire continents to find such a paradise.
It should be our duty that we, as people who can afford to live, help out those who cannot. We should live in gratitude for our own lives and in compassion for those of others.

![](ugurgallen.jpg)

Please take some time off your side and visit some of the following or similar web pages:  
[The Life You Can Save](https://www.thelifeyoucansave.org/causes-to-support/hunger-charities/)  
[Womankind Worldwide](https://www.womankind.org.uk/where-we-work/)  
[Save the Children](https://www.savethechildren.org/)  
[National Aids Trust](https://www.nat.org.uk/)  
[CombatStress](https://combatstress.org.uk/)