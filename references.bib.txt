@Manual{1,
	title = {World Happiness Report 2021},
	author = {{CCO: Public Domain}},
	year = {2021},
	url = {https://www.kaggle.com/ajaypalsinghlo/world-happiness-report-2021},
}

## I gave up on the bibliography file, it refused to work

@2 https://worldhappiness.report/ed/2020/the-nordic-exceptionalism-what-explains-why-the-nordic-countries-are-constantly-among-the-happiest-in-the-world/

@3 https://www.nytimes.com/2017/10/27/world/africa/burundi-international-criminal-court.html

@4 https://reliefweb.int/report/yemen/yemen-2019-humanitarian-needs-overview-enar

@5 https://www.amnesty.org/en/latest/press-release/2019/12/tanzania-withdrawal-of-individual-rights-to-african-court-will-deepen-repression/

@6 https://web.archive.org/web/20141105165537/http://www.helplesotho.org/lesotho/hivaids-in-lesotho/

@7 https://www.avert.org/professionals/hiv-around-world/sub-saharan-africa/botswana

@8 https://www.biicl.org/files/2216_tshosa_death_penalty_botswana.pdf

@9 https://www.voazimbabwe.com/a/zimbabwe-economy-battered-over-the-years/2724325.html

@10 https://2009-2017.state.gov/j/drl/rls/hrrpt/humanrightsreport/index.htm?year=2015&dlid=252745

@11 https://scholarworks.umass.edu/cgi/viewcontent.cgi?referer=&httpsredir=1&article=3591&context=theses

@12 http://www.jordantimes.com/news/local/population-stands-around-95-million-including-29-million-guests